$(document).ready(function(){

	$('.page-footer__back-to-top a').click(function () {

		$('body,html').animate({
			scrollTop: 0
		}, 500);

		return false;
	});
});